-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 16, 2018 at 11:35 PM
-- Server version: 5.5.45
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doccontrol`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `contact` varchar(300) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(300) NOT NULL,
  `address3` varchar(300) NOT NULL,
  `tel` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `taxid` varchar(80) NOT NULL,
  `website` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `contact`, `address1`, `address2`, `address3`, `tel`, `email`, `taxid`, `website`) VALUES
(1, 'บริษัท เดฟดี (ไทยแลนด์) จำกัด', 'คุณ อัครพล นุกูลวุฒิโอภาส', '18/26 ม.3 ต.บางเมือง อ.เมืองสมุทรปราการ 10270', '', '', '0851330199', 'akarapon@devdeethailand.com', '011 5559008 957 ( สำนักงานใหญ่ )', 'www.devdeethailand.com');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `code` varchar(30) NOT NULL,
  `name` varchar(300) NOT NULL,
  `contact` varchar(300) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(300) NOT NULL,
  `address3` varchar(300) NOT NULL,
  `tel` varchar(80) NOT NULL,
  `email` varchar(120) NOT NULL,
  `taxid` varchar(80) NOT NULL,
  `deleteflag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `code`, `name`, `contact`, `address1`, `address2`, `address3`, `tel`, `email`, `taxid`, `deleteflag`) VALUES
(1, 'code1', 'บริษัท ดีเอชแอล เอ็กซ์เพรส อินเตอร์เนชั่นแนล (ประเทศไทย) จำกัด', 'K. Taweepong Homlaor', '319 อาคารจัตุรัสจามจุรี ชั้น 22 และ 23', 'ถนนพญาไท แขวงปทุมวัน เขตปทุมวัน กรุงเทพฯ 10330', '-', '-', '-', '0105548000160 ( สำนักงานใหญ่ )', 0),
(2, 'code33333', '1', '2', '7', '8', '9', '4', '5', '3', 0),
(3, 'd', 'd', '', '', '', '', '', '', '', 0),
(4, 'd', 'd', '', '', '', '', '', '', '', 0),
(5, 'd', 's', '', '', '', '', '', '', '', 0),
(6, '', '', '', '', '', '', '', '', '', 0),
(7, '1', '', '', '', '', '', '', '', '', 0),
(8, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(9, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(10, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(11, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(12, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(13, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(14, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(15, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(16, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(17, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(18, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(19, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(20, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(21, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(22, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(23, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(24, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(25, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(26, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(27, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(28, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(29, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(30, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(31, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(32, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(33, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(34, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(35, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(36, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(37, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(38, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(39, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(40, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(41, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(42, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(43, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(44, '1', '2', '3', '4', '5', '6', '7', '9', '0', 0),
(45, '12', '3', '4', '23', '3', '2', '2', '12', '4', 0);

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `id` bigint(20) NOT NULL,
  `pro_id` bigint(20) NOT NULL,
  `pro_name` varchar(100) NOT NULL,
  `input_date` date NOT NULL,
  `item` varchar(100) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `type` int(11) NOT NULL,
  `note` varchar(300) NOT NULL,
  `deleteflag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`id`, `pro_id`, `pro_name`, `input_date`, `item`, `price`, `type`, `note`, `deleteflag`) VALUES
(1, 2, '', '0000-00-00', '', '0.00', 0, '', 1),
(2, 2, 'dd', '2018-07-14', 'test', '123.00', 1, '', 0),
(3, 2, 'dd', '2018-07-14', 'test', '100000.00', 2, 'test', 0),
(4, 1, 'test', '2018-07-14', 'tes', '21000.00', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` bigint(20) NOT NULL,
  `doc_ref` varchar(60) NOT NULL,
  `IssueDate` date NOT NULL,
  `IssueOrder` varchar(30) NOT NULL,
  `pro_id` int(11) NOT NULL DEFAULT '1',
  `cus_id` bigint(20) NOT NULL,
  `cus_name` varchar(300) NOT NULL,
  `due_date` varchar(11) NOT NULL,
  `cus_contact` varchar(120) NOT NULL,
  `cus_tel` varchar(120) NOT NULL,
  `cus_address` varchar(300) NOT NULL,
  `com_contact` varchar(120) NOT NULL,
  `com_tel` varchar(120) NOT NULL,
  `com_address` varchar(300) NOT NULL,
  `com_email` varchar(120) NOT NULL,
  `sub_total` decimal(16,2) NOT NULL,
  `vat` decimal(16,2) NOT NULL,
  `total` decimal(16,2) NOT NULL,
  `sub_alphabet` varchar(300) NOT NULL,
  `sign_name` varchar(300) NOT NULL,
  `sign_date` date NOT NULL,
  `payment` varchar(600) NOT NULL,
  `deleteflag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `doc_ref`, `IssueDate`, `IssueOrder`, `pro_id`, `cus_id`, `cus_name`, `due_date`, `cus_contact`, `cus_tel`, `cus_address`, `com_contact`, `com_tel`, `com_address`, `com_email`, `sub_total`, `vat`, `total`, `sub_alphabet`, `sign_name`, `sign_date`, `payment`, `deleteflag`) VALUES
(1, '', '2018-06-02', 'inv001', 1, 1, 'บริษัท ดีเอชแอล เอ็กซ์เพรส อินเตอร์เนชั่นแนล (ประเทศไทย) จำกัด', '60', 'K. Taweepong Homlaor', '-', '319 อาคารจัตุรัสจามจุรี ชั้น 22 และ 23\nถนนพญาไท แขวงปทุมวัน เขตปทุมวัน กรุงเทพฯ 10330\n-', 'บริษัท เดฟดี (ไทยแลนด์) จำกัด', '0851330199', '18/26 ม.3 ต.บางเมือง อ.เมืองสมุทรปราการ 10270\nundefined\n', 'akarapon@devdeethailand.com', '100.00', '7.00', '107.00', 'หนึ่งร้อยเจ็ดบาทถ้วน', 'test', '2018-06-02', '', 1),
(2, '', '2018-06-02', 'inv001', 1, 1, 'บริษัท ดีเอชแอล เอ็กซ์เพรส อินเตอร์เนชั่นแนล (ประเทศไทย) จำกัด', '60', 'K. Taweepong Homlaor', '-', '319 อาคารจัตุรัสจามจุรี ชั้น 22 และ 23\nถนนพญาไท แขวงปทุมวัน เขตปทุมวัน กรุงเทพฯ 10330\n-', 'บริษัท เดฟดี (ไทยแลนด์) จำกัด', '0851330199', '18/26 ม.3 ต.บางเมือง อ.เมืองสมุทรปราการ 10270\nundefined\n', 'akarapon@devdeethailand.com', '100.00', '7.00', '107.00', 'หนึ่งร้อยเจ็ดบาทถ้วน', 'test', '2018-06-02', 'test', 1),
(3, '', '2018-06-02', 'inv001', 1, 1, 'บริษัท ดีเอชแอล เอ็กซ์เพรส อินเตอร์เนชั่นแนล (ประเทศไทย) จำกัด', '60', 'K. Taweepong Homlaor', '-', '319 อาคารจัตุรัสจามจุรี ชั้น 22 และ 23\nถนนพญาไท แขวงปทุมวัน เขตปทุมวัน กรุงเทพฯ 10330\n-', 'บริษัท เดฟดี (ไทยแลนด์) จำกัด', '0851330199', '18/26 ม.3 ต.บางเมือง อ.เมืองสมุทรปราการ 10270', 'akarapon@devdeethailand.com', '435.00', '30.45', '465.45', 'สี่ร้อยหกสิบห้าบาทสี่สิบห้าสตางค์', 'dddf ssss', '2018-06-02', 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_detail`
--

CREATE TABLE `invoice_detail` (
  `id` bigint(20) NOT NULL,
  `inv_id` bigint(20) NOT NULL,
  `line_no` int(11) NOT NULL,
  `item_no` varchar(11) NOT NULL,
  `qty` varchar(100) NOT NULL,
  `unit` varchar(60) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `amount` decimal(16,2) NOT NULL,
  `item_desc` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_detail`
--

INSERT INTO `invoice_detail` (`id`, `inv_id`, `line_no`, `item_no`, `qty`, `unit`, `price`, `amount`, `item_desc`) VALUES
(1, 3, 1, '1', '1', '', '1.00', '1.00', 'aaaaaaaaaaaaa'),
(2, 3, 2, '', '', '', '0.00', '0.00', 'a'),
(3, 3, 3, '2', '2', '', '2.00', '2.00', 'ddadf'),
(4, 3, 4, '', '', '', '0.00', '0.00', 'ssssssss'),
(5, 3, 5, '', '', '', '0.00', '0.00', ''),
(6, 3, 6, '', '', '', '0.00', '0.00', ''),
(7, 3, 7, '', '', '', '0.00', '0.00', ''),
(8, 3, 8, '', '', '', '0.00', '0.00', ''),
(9, 3, 9, '', '', '', '0.00', '0.00', ''),
(10, 3, 10, '', '', '', '0.00', '0.00', ''),
(11, 3, 11, '3', '1', '', '2.00', '100.00', 'dsfsaf'),
(12, 3, 12, '', '', '', '0.00', '0.00', 'safdsafd'),
(13, 3, 13, '', '', '', '0.00', '0.00', 'sdddddddddd'),
(14, 3, 14, '', '', '', '0.00', '0.00', ''),
(15, 3, 15, '', '', '', '0.00', '0.00', ''),
(16, 3, 16, '', '', '', '0.00', '0.00', ''),
(17, 3, 17, '', '', '', '0.00', '0.00', ''),
(18, 3, 18, '', '', '', '0.00', '0.00', ''),
(19, 3, 19, '', '', '', '0.00', '0.00', ''),
(20, 3, 20, '', '', '', '0.00', '0.00', ''),
(21, 3, 21, '', '', '', '0.00', '0.00', ''),
(22, 3, 22, '', '', '', '0.00', '0.00', ''),
(23, 3, 23, '', '', '', '0.00', '0.00', ''),
(24, 3, 24, '', '', '', '0.00', '0.00', ''),
(25, 3, 25, '4', '', '', '1.00', '332.00', 'dddd');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` bigint(20) NOT NULL,
  `name` varchar(300) NOT NULL,
  `budget` double NOT NULL,
  `byear` int(11) NOT NULL,
  `note` varchar(300) NOT NULL,
  `deleteflag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `budget`, `byear`, `note`, `deleteflag`) VALUES
(1, 'test', 1234, 2018, 'ddd', 0),
(2, 'dd', 1123, 2017, '222', 0);

-- --------------------------------------------------------

--
-- Table structure for table `quatation`
--

CREATE TABLE `quatation` (
  `id` bigint(20) NOT NULL,
  `IssueDate` date NOT NULL,
  `IssueOrder` varchar(30) NOT NULL,
  `pro_id` int(11) NOT NULL DEFAULT '1',
  `cus_id` bigint(20) NOT NULL,
  `cus_name` varchar(300) NOT NULL,
  `due_date` varchar(11) NOT NULL,
  `cus_contact` varchar(120) NOT NULL,
  `cus_tel` varchar(120) NOT NULL,
  `cus_address` varchar(300) NOT NULL,
  `com_contact` varchar(120) NOT NULL,
  `com_tel` varchar(120) NOT NULL,
  `com_address` varchar(300) NOT NULL,
  `com_email` varchar(120) NOT NULL,
  `sub_total` decimal(16,2) NOT NULL,
  `vat` decimal(16,2) NOT NULL,
  `total` decimal(16,2) NOT NULL,
  `sub_alphabet` varchar(300) NOT NULL,
  `sign_name` varchar(300) NOT NULL,
  `sign_date` date NOT NULL,
  `payment` varchar(600) NOT NULL,
  `status` int(11) NOT NULL,
  `deleteflag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quatation`
--

INSERT INTO `quatation` (`id`, `IssueDate`, `IssueOrder`, `pro_id`, `cus_id`, `cus_name`, `due_date`, `cus_contact`, `cus_tel`, `cus_address`, `com_contact`, `com_tel`, `com_address`, `com_email`, `sub_total`, `vat`, `total`, `sub_alphabet`, `sign_name`, `sign_date`, `payment`, `status`, `deleteflag`) VALUES
(1, '2018-05-27', '', 2, 0, '', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 3, 1),
(2, '2018-05-27', 'qua001', 2, 2, '1', '30', '2', '4', '7\n8\n9', 'คุณ อัครพล นุกูลวุฒิโอภาส', '0851330199', '18/26 ม.3 ต.บางเมือง อ.เมืองสมุทรปราการ 10270', 'akarapon@devdeethailand.com', '100.00', '107.00', '107.00', 'หนึ่งร้อยเจ็ดบาทถ้วน', 'มนต์ชัย ลาภผลโอฬาร', '2018-05-27', '30% of project \n10% of project\nma %ddddd', 3, 0),
(3, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 1, 0),
(4, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 0, 0),
(5, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 0, 0),
(6, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 0, 0),
(7, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 0, 0),
(8, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 0, 0),
(9, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 0, 0),
(10, '2018-05-27', '', 1, 1, '12', '0', '2', '4', '7\n8\n9ddd', 'tddd', 'dfsaf', 'dd\nundefined\nfsa', '', '0.00', '0.00', '0.00', '', '', '2018-05-27', '', 0, 0),
(11, '2018-07-16', 't', 1, 1, 'บริษัท ดีเอชแอล เอ็กซ์เพรส อินเตอร์เนชั่นแนล (ประเทศไทย) จำกัด', '6', 'K. Taweepong Homlaor', '-', '319 อาคารจัตุรัสจามจุรี ชั้น 22 และ 23\nถนนพญาไท แขวงปทุมวัน เขตปทุมวัน กรุงเทพฯ 10330\n-', 'บริษัท เดฟดี (ไทยแลนด์) จำกัด', '0851330199', '18/26 ม.3 ต.บางเมือง อ.เมืองสมุทรปราการ 10270\n\n', 'akarapon@devdeethailand.com', '1.00', '0.07', '1.07', 'เอ็ดบาทเจ็ดสตางค์', 'test', '2018-07-16', '', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quatation_detail`
--

CREATE TABLE `quatation_detail` (
  `id` bigint(20) NOT NULL,
  `qt_id` bigint(20) NOT NULL,
  `line_no` int(11) NOT NULL,
  `item_no` varchar(11) NOT NULL,
  `qty` varchar(100) NOT NULL,
  `unit` varchar(60) NOT NULL,
  `price` decimal(16,2) NOT NULL,
  `amount` decimal(16,2) NOT NULL,
  `item_desc` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quatation_detail`
--

INSERT INTO `quatation_detail` (`id`, `qt_id`, `line_no`, `item_no`, `qty`, `unit`, `price`, `amount`, `item_desc`) VALUES
(1, 10, 1, '0', '', '', '0.00', '0.00', ''),
(2, 10, 2, '0', '', '', '0.00', '0.00', ''),
(3, 10, 3, '0', '', '', '0.00', '0.00', ''),
(4, 10, 4, '0', '', '', '0.00', '0.00', ''),
(5, 10, 5, '0', '', '', '0.00', '0.00', ''),
(6, 10, 6, '0', '', '', '0.00', '0.00', ''),
(7, 10, 7, '0', '', '', '0.00', '0.00', ''),
(8, 10, 8, '0', '', '', '0.00', '0.00', ''),
(9, 10, 9, '0', '', '', '0.00', '0.00', ''),
(10, 10, 10, '0', '', '', '0.00', '0.00', ''),
(11, 10, 11, '0', '', '', '0.00', '0.00', ''),
(12, 10, 12, '0', '', '', '0.00', '0.00', ''),
(13, 10, 13, '0', '', '', '0.00', '0.00', ''),
(14, 10, 14, '0', '', '', '0.00', '0.00', ''),
(15, 10, 15, '0', '', '', '0.00', '0.00', ''),
(16, 10, 16, '0', '', '', '0.00', '0.00', ''),
(17, 10, 17, '0', '', '', '0.00', '0.00', ''),
(18, 10, 18, '0', '', '', '0.00', '0.00', ''),
(19, 10, 19, '0', '', '', '0.00', '0.00', ''),
(20, 10, 20, '0', '', '', '0.00', '0.00', ''),
(21, 10, 21, '0', '', '', '0.00', '0.00', ''),
(22, 10, 22, '0', '', '', '0.00', '0.00', ''),
(23, 10, 23, '0', '', '', '0.00', '0.00', ''),
(24, 10, 24, '0', '', '', '0.00', '0.00', ''),
(25, 2, 1, '1', '1', 'ea', '2222.00', '2222.00', 'test'),
(26, 2, 2, '', '', '', '0.00', '0.00', ''),
(27, 2, 3, '2', '1', 'pcs', '2.00', '2.00', 'testddddd'),
(28, 2, 4, '', '', '', '0.00', '0.00', 'sfdsfsf'),
(29, 2, 5, '', '', '', '0.00', '0.00', 'safdsdfsfdfs'),
(30, 2, 6, '', '', '', '0.00', '0.00', ''),
(31, 2, 7, '', '', '', '0.00', '0.00', ''),
(32, 2, 8, '', '', '', '0.00', '0.00', ''),
(33, 2, 9, '', '', '', '0.00', '0.00', ''),
(34, 2, 10, '', '', '', '0.00', '0.00', ''),
(35, 2, 11, '', '', '', '0.00', '0.00', ''),
(36, 2, 12, '', '', '', '0.00', '0.00', ''),
(37, 2, 13, '', '', '', '0.00', '0.00', ''),
(38, 2, 14, '', '', '', '0.00', '0.00', ''),
(39, 2, 15, '', '', '', '0.00', '0.00', ''),
(40, 2, 16, '', '', '', '0.00', '0.00', ''),
(41, 2, 17, '', '', '', '0.00', '0.00', ''),
(42, 2, 18, '', '', '', '0.00', '0.00', ''),
(43, 2, 19, '', '', '', '0.00', '0.00', ''),
(44, 2, 20, '', '', '', '0.00', '0.00', ''),
(45, 2, 21, '', '', '', '0.00', '0.00', ''),
(46, 2, 22, '', '', '', '0.00', '0.00', ''),
(47, 2, 23, '', '', '', '0.00', '0.00', ''),
(48, 2, 24, '', '', '', '0.00', '0.00', ''),
(49, 2, 25, '', '', '', '0.00', '0.00', ''),
(50, 11, 1, '1', '', '', '1.00', '1.00', '2f'),
(51, 11, 2, '', '', '', '0.00', '0.00', ''),
(52, 11, 3, '', '', '', '0.00', '0.00', ''),
(53, 11, 4, '', '', '', '0.00', '0.00', ''),
(54, 11, 5, '', '', '', '0.00', '0.00', ''),
(55, 11, 6, '', '', '', '0.00', '0.00', ''),
(56, 11, 7, '', '', '', '0.00', '0.00', ''),
(57, 11, 8, '', '', '', '0.00', '0.00', ''),
(58, 11, 9, '', '', '', '0.00', '0.00', ''),
(59, 11, 10, '', '', '', '0.00', '0.00', ''),
(60, 11, 11, '', '', '', '0.00', '0.00', ''),
(61, 11, 12, '', '', '', '0.00', '0.00', ''),
(62, 11, 13, '', '', '', '0.00', '0.00', ''),
(63, 11, 14, '', '', '', '0.00', '0.00', ''),
(64, 11, 15, '', '', '', '0.00', '0.00', ''),
(65, 11, 16, '', '', '', '0.00', '0.00', ''),
(66, 11, 17, '', '', '', '0.00', '0.00', ''),
(67, 11, 18, '', '', '', '0.00', '0.00', ''),
(68, 11, 19, '', '', '', '0.00', '0.00', ''),
(69, 11, 20, '', '', '', '0.00', '0.00', ''),
(70, 11, 21, '', '', '', '0.00', '0.00', ''),
(71, 11, 22, '', '', '', '0.00', '0.00', ''),
(72, 11, 23, '', '', '', '0.00', '0.00', ''),
(73, 11, 24, '', '', '', '0.00', '0.00', ''),
(74, 11, 25, '', '', '', '0.00', '0.00', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `user` varchar(120) NOT NULL,
  `password` varchar(300) NOT NULL,
  `deleteflag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user`, `password`, `deleteflag`) VALUES
(1, 'DevDee', 'a5057caa4faef6986396df87cb8b2d05', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vat_doc`
--

CREATE TABLE `vat_doc` (
  `id` bigint(20) NOT NULL,
  `pro_id` bigint(20) NOT NULL,
  `book_no` varchar(40) NOT NULL,
  `num_no` varchar(40) NOT NULL,
  `pay_name` varchar(120) NOT NULL,
  `pay_id` varchar(40) NOT NULL,
  `pay_address` varchar(300) NOT NULL,
  `com_name` varchar(120) NOT NULL,
  `com_id` varchar(40) NOT NULL,
  `com_address` varchar(300) NOT NULL,
  `salary_year` varchar(40) NOT NULL,
  `salary_pay` decimal(16,2) NOT NULL,
  `salary_wht` decimal(16,2) NOT NULL,
  `fee_year` varchar(40) NOT NULL,
  `fee_pay` decimal(16,2) NOT NULL,
  `fee_wht` decimal(16,2) NOT NULL,
  `copyfee_year` varchar(40) NOT NULL,
  `copyfee_pay` decimal(16,2) NOT NULL,
  `copyfee_wht` decimal(16,2) NOT NULL,
  `interest_year` varchar(40) NOT NULL,
  `interest_pay` decimal(16,2) NOT NULL,
  `interest_wht` decimal(16,2) NOT NULL,
  `interest1_year` varchar(40) NOT NULL,
  `interest1_pay` decimal(16,2) NOT NULL,
  `interest1_wht` decimal(16,2) NOT NULL,
  `interest2_year` varchar(40) NOT NULL,
  `interest2_pay` decimal(16,2) NOT NULL,
  `interest2_wht` decimal(16,2) NOT NULL,
  `interest3_year` varchar(40) NOT NULL,
  `interest3_pay` decimal(16,2) NOT NULL,
  `interest3_wht` decimal(16,2) NOT NULL,
  `interest4_year` varchar(40) NOT NULL,
  `interest4_pay` decimal(16,2) NOT NULL,
  `interest4_wht` decimal(16,2) NOT NULL,
  `interest21_year` varchar(40) NOT NULL,
  `interest21_pay` decimal(16,2) NOT NULL,
  `interest21_wht` decimal(16,2) NOT NULL,
  `interest22_year` varchar(40) NOT NULL,
  `interest22_pay` decimal(16,2) NOT NULL,
  `interest22_wht` decimal(16,2) NOT NULL,
  `interest23_year` varchar(40) NOT NULL,
  `interest23_pay` decimal(16,2) NOT NULL,
  `interest23_wht` decimal(16,2) NOT NULL,
  `interest24_year` varchar(40) NOT NULL,
  `interest24_pay` decimal(16,2) NOT NULL,
  `interest24_wht` decimal(16,2) NOT NULL,
  `interest25_year` varchar(40) NOT NULL,
  `interest25_pay` decimal(16,2) NOT NULL,
  `interest25_wht` decimal(16,2) NOT NULL,
  `interest25_comment` varchar(60) NOT NULL,
  `revenue_year` varchar(40) NOT NULL,
  `revenue_pay` decimal(16,2) NOT NULL,
  `revenue_wht` decimal(16,2) NOT NULL,
  `revenue_comment` varchar(60) NOT NULL,
  `other_year` varchar(40) NOT NULL,
  `other_pay` decimal(16,2) NOT NULL,
  `other_wht` decimal(16,2) NOT NULL,
  `other_comment` varchar(60) NOT NULL,
  `total_pay` decimal(16,2) NOT NULL,
  `total_wht` decimal(16,2) NOT NULL,
  `total_alphabet` varchar(300) NOT NULL,
  `onetime_vat` int(11) NOT NULL,
  `forever_vat` int(11) NOT NULL,
  `wht_vat` int(11) NOT NULL,
  `other_vat` int(11) NOT NULL,
  `other_vat_comment` varchar(60) NOT NULL,
  `social` decimal(16,2) NOT NULL,
  `fund` decimal(16,2) NOT NULL,
  `sign_name` varchar(300) NOT NULL,
  `sign_date` date NOT NULL,
  `deleteflag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vat_doc`
--

INSERT INTO `vat_doc` (`id`, `pro_id`, `book_no`, `num_no`, `pay_name`, `pay_id`, `pay_address`, `com_name`, `com_id`, `com_address`, `salary_year`, `salary_pay`, `salary_wht`, `fee_year`, `fee_pay`, `fee_wht`, `copyfee_year`, `copyfee_pay`, `copyfee_wht`, `interest_year`, `interest_pay`, `interest_wht`, `interest1_year`, `interest1_pay`, `interest1_wht`, `interest2_year`, `interest2_pay`, `interest2_wht`, `interest3_year`, `interest3_pay`, `interest3_wht`, `interest4_year`, `interest4_pay`, `interest4_wht`, `interest21_year`, `interest21_pay`, `interest21_wht`, `interest22_year`, `interest22_pay`, `interest22_wht`, `interest23_year`, `interest23_pay`, `interest23_wht`, `interest24_year`, `interest24_pay`, `interest24_wht`, `interest25_year`, `interest25_pay`, `interest25_wht`, `interest25_comment`, `revenue_year`, `revenue_pay`, `revenue_wht`, `revenue_comment`, `other_year`, `other_pay`, `other_wht`, `other_comment`, `total_pay`, `total_wht`, `total_alphabet`, `onetime_vat`, `forever_vat`, `wht_vat`, `other_vat`, `other_vat_comment`, `social`, `fund`, `sign_name`, `sign_date`, `deleteflag`) VALUES
(1, 0, '', '', '', '', '', '', '', '', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '', '0.00', '0.00', '', '', '0.00', '0.00', '', '0.00', '0.00', '', 0, 0, 0, 0, '', '0.00', '0.00', '', '0000-00-00', 1),
(2, 0, '', '', '', '', '', '', '', '', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '', '0.00', '0.00', '', '', '0.00', '0.00', '', '0.00', '0.00', '', 0, 0, 0, 0, '', '0.00', '0.00', '', '2018-06-03', 1),
(3, 1, '1', '001', 'บริษัท เดฟดี (ไทยแลนด์) จำกัด', '0115559008957', '18/26 ม.3 ต.บางเมือง อ.เมืองสมุทรปราการ 10270\n\n', 'มนต์ชัย ลาภผลโอฬาร', '3101501227762', '93/3 ตากสิน', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', '', '0.00', '0.00', 'test', '', '0.00', '0.00', 'ddd', '', '0.00', '0.00', 'ohter ', '30000.00', '3000.00', 'สามพันบาทถ้วน', 0, 0, 0, 0, 'dddddddddd', '750.00', '900.00', 'Monchai', '2018-06-03', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_detail`
--
ALTER TABLE `invoice_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quatation`
--
ALTER TABLE `quatation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quatation_detail`
--
ALTER TABLE `quatation_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vat_doc`
--
ALTER TABLE `vat_doc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `invoice_detail`
--
ALTER TABLE `invoice_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `quatation`
--
ALTER TABLE `quatation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `quatation_detail`
--
ALTER TABLE `quatation_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vat_doc`
--
ALTER TABLE `vat_doc`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

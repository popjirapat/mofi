<?php
  
class QuatationDetailModel extends CI_Model {
	
    private $tbl_name = 'quatation_detail';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getQuatationDetailById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function listUpdate($id, $listData){
		 
		$nResult  = 0;
		foreach ($listData as $key => $data)
		{
			$data['qt_id'] = $id;
			$data['price']  = str_replace("," , "" , $data['price']);
			$data['amount']  = str_replace("," , "" , $data['amount']);
			if ($data['id'] == 0) {  
    			$nResult = $this->insert($data); 
		    }
		    else {  
		      	$nResult = $this->update($data['id'], $data);
		    }		
		}
        return $nResult;
    }
	
 
	public function getQuatationDetailModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('id', $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getQuatationDetailListById($id){
		
        $this->db->where('qt_id', $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		/*if(isset($dataModel['name']) && $dataModel['name'] != ""){
		 	$sql .= " and name like '%".$this->db->escape_str( $dataModel['name'])."%' ";
		}
		
		if(isset($dataModel['branch']) && $dataModel['branch'] != ""){
		 	$sql .= " and branch like '%".$this->db->escape_str( $dataModel['branch'])."%' ";
		}
		
		if(isset($dataModel['contact']) && $dataModel['contact'] != ""){
		 	$sql .= " and contact like '%".$this->db->escape_str( $dataModel['contact'])."%' ";
		}
		
		if(isset($dataModel['email']) && $dataModel['email'] != ""){
		 	$sql .= " and email like '%".$this->db->escape_str( $dataModel['email'])."%' ";
		}*/
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getQuatationDetailModelList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT ".$offset.", ".$limit;
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteQuatationDetail($id){
		$result = false;
		try{
			$query = $this->getQuatationDetailById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{ 
				$modelData = array(  
					'deleteflag' => 1  
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData); 
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getQuatationDetailComboList(){
		
		$sql = "SELECT id, 	IssueOrder, IssueDate, cus_name FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
}
?>
<?php
  
class CustomerModel extends CI_Model {
	
    private $tbl_name = 'customer';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getCustomerNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getCustomerNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('Customer_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getCustomerModel($id){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		$this->db->where('deleteflag', 0);
		$this->db->where($this->id, $id);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['code']) && $dataModel['code'] != ""){
		 	$sql .= " and code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		}
		
		if(isset($dataModel['name']) && $dataModel['name'] != ""){
		 	$sql .= " and name like '%".$this->db->escape_str( $dataModel['name'])."%' ";
		}
		
		if(isset($dataModel['contact']) && $dataModel['contact'] != ""){
		 	$sql .= " and contact like '%".$this->db->escape_str( $dataModel['contact'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getCustomerNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteCustomername($id){
		$result = false;
		try{
			$query = $this->getCustomerNameById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'deleteflag' => 1 //$row->Customer_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getCustomerComboList(){
		
		$sql = "SELECT id, 	name, contact, address1, address2, address3, tel, email, taxid  FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>
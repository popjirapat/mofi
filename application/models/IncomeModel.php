<?php
  
class IncomeModel extends CI_Model {
	
    private $tbl_name = 'income';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getIncomeNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getIncomeNameAllList(){ 
		$this->db->where('deleteflag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getIncomeModel(){ 
		$this->db->where('deleteflag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['pro_id']) && $dataModel['pro_id'] > 0){
		 	$sql .= " and pro_id = '".$dataModel['pro_id']."' ";
		}
		
		if(isset($dataModel['type']) && $dataModel['type'] != ""){
		 	$sql .= " and type = '".$dataModel['type']."' ";
		}
		
		// if(isset($dataModel['contact']) && $dataModel['contact'] != ""){
		 	// $sql .= " and contact like '%".$this->db->escape_str( $dataModel['contact'])."%' ";
		// }
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getIncomeNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);	
		
		 
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$sql .= " LIMIT $offset, $limit";
		
		//print($sql );
		 
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
 
	public function deleteIncomename($id){
		$result = false;
		try{
			$query = $this->getIncomeNameById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'deleteflag' => 1 //$row->Income_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData); 
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getIncomeComboList(){
		
		$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
	public function getTotalReportModelList(){
		
		$sql = "SELECT pt.id, pt.name, inc.type, sum(inc.price) as price FROM project pt LEFT JOIN income inc on pt.id = inc.pro_id WHERE pt.deleteflag = 0 group by pt.id, inc.type ORDER BY pt.name ";
		$query = $this->db->query($sql);
		
		$resultData = array();
		$ids = array();
		
		foreach ($query->result() as $row)
		{
			if(!isset($resultData[$row->id])){
				$resultData[$row->id]['project'] =  $row->name;		
				$resultData[$row->id]['expense'] = "0.00";
				$resultData[$row->id]['income'] = "0.00";
				
			}
			
			if($row->type == 2){
				$resultData[$row->id]['expense'] = $row->price;				
			}else if($row->type == 1){
				$resultData[$row->id]['income'] = $row->price;
			} 
		}
		
		foreach($resultData as $dtData){
			$ids[] =  $dtData;
		}
		
		return $ids;
		return  $resultData;
	}
}
?>
<?php
  
class VatModel extends CI_Model {
	
    private $tbl_name = 'vat_doc';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getVatNameById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getVatNameAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        $this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('Vat_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getVatModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('Vat_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//print_r($dataModel);
		
		if(isset($dataModel['com_name']) && $dataModel['com_name'] != ""){
		 	$sql .= " and vat.com_name like '%".$this->db->escape_str( $dataModel['com_name'])."%' ";
		}
		
		if(isset($dataModel['num_no']) && $dataModel['num_no'] != ""){
		 	$sql .= " and vat.num_no like '%".$this->db->escape_str( $dataModel['num_no'])."%' ";
		}
		
		if(isset($dataModel['pro_id']) && $dataModel['pro_id'] > 0){
		 	$sql .= " and vat.pro_id =  ".$dataModel['pro_id'] ;
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." vat WHERE deleteflag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getVatNameList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT vat.id, vat.pro_id, vat.book_no, vat.num_no, vat.pay_name, vat.pay_id, vat.pay_address, vat.com_name, vat.com_id, vat.com_address, vat.salary_year, vat.salary_pay, vat.salary_wht, vat.fee_year, vat.fee_pay, vat.fee_wht, vat.copyfee_year, vat.copyfee_pay, vat.copyfee_wht, vat.interest_year, vat.interest_pay, vat.interest_wht, vat.interest1_year, vat.interest1_pay, vat.interest1_wht, vat.interest2_year, vat.interest2_pay, vat.interest2_wht, vat.interest3_year, vat.interest3_pay, vat.interest3_wht, vat.interest4_year, vat.interest4_pay, vat.interest4_wht, vat.interest21_year, vat.interest21_pay, vat.interest21_wht, vat.interest22_year, vat.interest22_pay, vat.interest22_wht, vat.interest23_year, vat.interest23_pay, vat.interest23_wht, vat.interest24_year, vat.interest24_pay, vat.interest24_wht, vat.interest25_year, vat.interest25_pay, vat.interest25_wht, vat.interest25_comment, vat.revenue_year, vat.revenue_pay, vat.revenue_wht, vat.revenue_comment, vat.other_year, vat.other_pay, vat.other_wht, vat.other_comment, vat.total_pay, vat.total_wht, vat.total_alphabet, vat.onetime_vat, vat.forever_vat, vat.wht_vat, vat.other_vat, vat.other_vat_comment, vat.social, vat.fund, vat.sign_name, vat.sign_date, pj.name as project_name   FROM ". $this->tbl_name  . "  vat LEFT JOIN project pj on vat.pro_id = pj.id WHERE vat.deleteflag = 0  "; 
		
		$sql =  $this->getSearchQuery($sql, $dataModel);
		  
		// if($order != ""){
			// $this->db->order_by($order, $direction);
		// }else{
			// $this->db->order_by($this->id ,$direction); 
		// }
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		//echo $sql;
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['Vat_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}		
	
	public function deleteVatname($id){
		$result = false;
		try{
			$query = $this->getVatNameById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					//'update_date' => date("Y-m-d H:i:s"),
					//'update_user' => $this->session->userdata('user_name'),
					'deleteflag' => 1 //$row->Project_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getVatComboList(){
		
		$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE deleteflag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>
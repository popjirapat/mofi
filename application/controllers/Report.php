<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct() {
        parent::__construct();
		if(! $this->session->userdata('validated')){
            redirect('login');
        }
    }
	
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('report/report_view'); 
		$this->load->view('share/footer');
	}
	
	public function reportTotal()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('report/reporttotal_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function getReportModelList(){
	 
		try{ 
			$this->load->model('IncomeModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  1;
			$PageSize =   2000;
			$direction =  "input_date";
			$SortOrder = "desc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = 0;
			 
			$result['status'] = true;
			$result['message'] = $this->IncomeModel->getIncomeNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->IncomeModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getTotalReportModelList(){
	 
		try{ 
			$this->load->model('IncomeModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$result['status'] = true;
			$result['message'] = $this->IncomeModel->getTotalReportModelList();
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}

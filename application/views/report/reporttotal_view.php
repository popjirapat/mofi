<script src="<?php echo base_url('asset/reportTotalController.js');?>"></script>
<div  ng-controller="reportTotalController" ng-init="onInit()">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $this->lang->line('Report');?> <?php echo $this->lang->line('Income');?></h1>
	</div>
	<!-- /.col-lg-12 -->
	
	<!-- /List.row types-->
		<div class="row " >
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"> 
					</div> 
					<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-xs-12 DisplayDevice"  >
						<div class="table-responsive"> 
							<table class="table table-striped" style="max-width:900px;" >
								<thead> 
									<tr>  
										<th><?php echo $this->lang->line('Project');?></th> 
										<th><?php echo $this->lang->line('revenue');?></th>
										<th><?php echo $this->lang->line('expenditure');?></th>
										<th><?php echo $this->lang->line('Total');?></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in modelDeviceList">
										<td ng-bind="item.project"></td>  
										<td class="text-right" ng-bind="addCommas(item.income)"></td>
										<td class="text-right" ng-bind="addCommas(item.expense)"></td>
										<td class="text-right" ng-class="showColor(item)" ng-bind="showPrice(item)"></td>  
									</tr>
									<tr>
										<td  class="text-right"  ><?php echo $this->lang->line('Total');?>  </td> 
										<td class="text-right " ng-class="showColorEx()"  ng-bind="total" ></td>
										<td class="text-right " ng-class="showColorEx()"  ng-bind="expense" ></td>
										<td class="text-right " ng-class="showColorEx()"  ng-bind="netprice" ></td>
									</tr>
								</tbody>								
							</table>
						</div>
						<!-- /.table-responsive -->
					</div> 
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /List.row types-->
	
</div>
  
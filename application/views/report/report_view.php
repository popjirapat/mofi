<script src="<?php echo base_url('asset/reportController.js');?>"></script>
<div  ng-controller="reportController" ng-init="onInit()">
	<div class="col-lg-12">
		<h1 class="page-header"><?php echo $this->lang->line('Report');?> <?php echo $this->lang->line('Income');?></h1>
	</div>
	<!-- /.col-lg-12 -->
	
	<!-- /List.row types-->
		<div class="row " >
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"> 
					</div> 
					<div class="panel-body">
					<div class="form-group col-lg-12 col-md-12 col-xs-12">
						<div class="col-lg-6 col-md-6 col-xs-12">
							<label><?php echo $this->lang->line('ProjectName');?></label>
							<ui-select ng-model="TempSearchProjectIndex.selected" theme="selectize">
								<ui-select-match>{{$select.selected.name}}</ui-select-match>
								<ui-select-choices repeat="pIndex in listProject | filter: $select.search">
									<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
								</ui-select-choices>
							</ui-select> 
						</div> 
						<div class="col-lg-6 col-md-6 col-xs-12">	
							<br/>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
						</div> 
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12 DisplayDevice" style="display:none;" >
						<div class="table-responsive"> 
							<table class="table table-striped" style="max-width:900px;" >
								<thead>
									<tr>
										<th colspan="3"><?php echo $this->lang->line('ProjectName');?>  {{TempSearchProjectIndex.selected.name}}</th>
									</tr>
									<tr>  
										<th><?php echo $this->lang->line('Date');?></th>
										<?php /*<th><?php echo $this->lang->line('Project');?></th>*/ ?>
										<th><?php echo $this->lang->line('Item');?></th>
										<th><?php echo $this->lang->line('Price');?></th>   
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in modelDeviceList">
										<td ng-bind="item.input_date"></td> 
										<?php // <td ng-bind="item.pro_name"></td> ?>
										<td ng-bind="item.item"></td>
										<td class="text-right" ng-class="showColor(item)" ng-bind="showPrice(item)"></td>  
									</tr>
									<tr>
										<td colspan="2" class="text-right"  ><?php echo $this->lang->line('Total');?>  </td> 
										<td class="text-right " ng-class="showColorEx()"  ng-bind="netprice" ></td> 
									</tr>
								</tbody>								
							</table>
						</div>
						<!-- /.table-responsive -->
					</div> 
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /List.row types-->
	
</div>
  
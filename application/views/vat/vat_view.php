<script src="<?php echo base_url('asset/vatController.js');?>"></script>
<div  ng-controller="vatController" ng-init="onInit()">
 <div class="row">
	<ul class="navigator">
		<?php /*<li class="nav"><a href="/Rooms"><?php echo $this->lang->line('Customer');?></a></li>*/ ?>
		<li class="nav_active"> <?php echo $this->lang->line('VatBook');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $this->lang->line('VatBook');?></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	
	
		<!-- /List.row types-->
	<div class="row  SearchDevice" style="display:none;">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('Search');?>
				</div> 
				<div class="panel-body">
				<div class="form-group col-lg-12 col-md-12 col-xs-12">
					<div class="col-lg-6 col-md-6 col-xs-12">
						<label><span class="text-danger" >*</span><?php echo $this->lang->line('NumNo');?></label>
						<input class="form-control" ng-model="modelSearch.num_no" maxlength="80" >
					</div> 
					<div class="col-lg-6 col-md-6 col-xs-12"> 
						<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyNameEx2');?></label>
						<input class="form-control" ng-model="modelSearch.com_name" maxlength="80" >
					</div> 
				</div>
				<div class="form-group col-lg-12 col-md-12 col-xs-12">
					<div class="col-lg-6 col-md-6 col-xs-12">
						<label><?php echo $this->lang->line('ProjectName');?></label>
						<ui-select ng-model="TempSearchProjectIndex.selected" theme="selectize">
							<ui-select-match>{{$select.selected.name}}</ui-select-match>
							<ui-select-choices repeat="pIndex in listProject | filter: $select.search">
								<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
							</ui-select-choices>
						</ui-select> 
					</div> 
					<div class="col-lg-6 col-md-6 col-xs-12"> 
					</div> 
				</div>
				<div class="col-lg-12 col-md-12 col-xs-12">
					<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
					<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
					<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
				</div>
				</div> 
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /List.row types-->
      		  
	<!-- / create vat  -->
	<div class="row addDevice" style="display:none;">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo $this->lang->line('VatBook');?>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div role="form">
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-6"> 
										<label><input type="checkbox" ng-model="fullset" >&nbsp; แบบเต็ม</label> 
									</div><div class="col-lg-6 col-md-6 col-xs-6"> 
									 
									</div>								
								</div>
								<div class="form-group col-lg-12 col-md-12 col-xs-12">
									<div class="col-lg-6 col-md-6 col-xs-6"> 
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('ProjectName');?></label>
										<ui-select ng-model="TempProjectIndex.selected" theme="selectize">
											<ui-select-match>{{$select.selected.name}}</ui-select-match>
											<ui-select-choices repeat="pIndex in listProject | filter: $select.search">
												<span ng-bind-html="pIndex.name  | highlight: $select.search"></span>
											</ui-select-choices>
										</ui-select> 
										<p class="CreateModel_cus_id require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-3 col-md-3 col-xs-3">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('BookNo');?></label>
										<input class="form-control" ng-model="CreateModel.book_no">
										<p class="CreateModel_book_no require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-3 col-md-3 col-xs-3">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('NumNo');?></label>
										<input class="form-control" ng-model="CreateModel.num_no">
										<p class="CreateModel_num_no require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div> 
								<div class="form-group ">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyNameEx');?></label>
										<input class="form-control" ng-model="CreateModel.pay_name">
										<p class="CreateModel_pay_name require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyID');?></label>
										<input class="form-control" ng-model="CreateModel.pay_id" maxlength=13>
										<p class="CreateModel_pay_id require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>  
								<div class="form-group">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address');?></label>
										<textarea class="form-control" ng-model="CreateModel.pay_address" rows="3"></textarea>
										<br/>
										<p class="CreateModel_pay_address require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>
								<div class="form-group ">
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyNameEx2');?></label>
										<input class="form-control" ng-model="CreateModel.com_name">
										<p class="CreateModel_com_name require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
									<div class="col-lg-6 col-md-6 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyID');?></label>
										<input class="form-control" ng-model="CreateModel.com_id"  maxlength=13>
										<p class="CreateModel_com_id require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>  
								<div class="form-group">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<label><span class="text-danger" >*</span><?php echo $this->lang->line('Address');?></label>
										<textarea class="form-control" ng-model="CreateModel.com_address" rows="3"></textarea>
										<br/>
										<p class="CreateModel_com_address require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>
								
								
								<div class="form-group ">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>ปรเะเภทเงินได้พึงประเมินที่จ่าย</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<label>วันเดือนหรือปีภาษีที่จ่าย</label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<label>จำนวนเงินที่จ่าย</label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<label>ภาษีที่หักและนำส่งไว้</label> 
									</div> 
								</div>
								<div class="form-group col-lg-12">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>1. เงินเดือน ค่าจ้าง เบี้ยเลี้ยง โบนัส ฯลฯ ตามมาตรา 40(1)</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.salary_year">
										<p class="CreateModel_salary_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.salary_pay">
										<p class="CreateModel_salary_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.salary_wht">
										<p class="CreateModel_salary_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>
								<div class="form-group col-lg-12">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>2. ค่าธรรมเนียม ค่านายหน้า ฯลฯ ตามาตรา 40 (2)</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.fee_year">
										<p class="CreateModel_fee_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.fee_pay">
										<p class="CreateModel_fee_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.fee_wht">
										<p class="CreateModel_fee_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>
								<div class="form-group col-lg-12" ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>3. ค่าแห่งลิขสิทธิ์ ฯลฯ ตามมาตรา 40 (3)</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.copyfee_year">
										<p class="CreateModel_copyfee_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										 <input class="form-control" ng-model="CreateModel.copyfee_pay">
										<p class="CreateModel_copyfee_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.copyfee_wht">
										<p class="CreateModel_copyfee_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>4. (ก) ดอกเบี้ย ฯลฯ ตามมาตรา 40(4) (ก)</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_year">
										<p class="CreateModel_interest_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_pay">
										<p class="CreateModel_interest_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_wht">
										<p class="CreateModel_interest_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label> (ข) เงินปันผลเงินส่วนแบ่งกำไร ฯลฯ ตามาตรา 40 (4) (ข)</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>  (1) กรณีผู้ได้รับเงินปันผลได้รับเครดิตภาษี โดยจ่ายจาก</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div> 
								</div> <div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>  กำไรสุทธิของกิจการที่ต้องเสียภาษีเงินได้นิติบุคคลในอัตราดังนี้</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>  (1.1) อัตราร้อยละ 30 ของกำไรสุทธิ</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_year1">
										<p class="CreateModel_interest_year1 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_pay1">
										<p class="CreateModel_interest_pay1 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_wht1">
										<p class="CreateModel_interest_wht1 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>   (1.2) อัตราร้อยละ 25 ของกำไรสุทธิ</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_year2">
										<p class="CreateModel_interest_year2 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_pay2">
										<p class="CreateModel_interest_pay2 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_wht2">
										<p class="CreateModel_interest_wht2 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>   (1.3) อัตราร้อยละ 20 ของกำไรสุทธิ</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_year3">
										<p class="CreateModel_interest_year3 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_pay3">
										<p class="CreateModel_interest_pay3 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_wht3">
										<p class="CreateModel_interest_wht3 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div>  <div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label> (1.4) อัตราอื่น ๆ ระบุ _____________ ของกำไรสุทธิ</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_year4">
										<p class="CreateModel_interest_year4 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_pay4">
										<p class="CreateModel_interest_pay4 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest_wht4">
										<p class="CreateModel_interest_wht4 require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label> (2) กรณีผู้ได้รับเงินปันผลไม่ได้รับเครดิตภาษี เนื่องจากจ่ายจาก</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>    (2.1) กำไรสุทธิของกิจการที่ได้รับยกเว้น</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest21_year">
										<p class="CreateModel_interest21_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest21_pay">
										<p class="CreateModel_interest21_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest21_wht">
										<p class="CreateModel_interest21_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>   (2.2) เงินปันผลหรือเงินส่วนแบ่งของกำไรที่ได้รับยกเว้นไม่ต้อง</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>   นำมารวมคำนวณเป็นรายได้เพื่อเสียภาษีเงินได้นิติบุคคล</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest22_year">
										<p class="CreateModel_interest22_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest22_pay">
										<p class="CreateModel_interest22_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest22_wht">
										<p class="CreateModel_interest22_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>   (2.3) กำไรสุทธิส่วนที่ได้หักผลขาดทุนสุทธิยกมาไม่เกิน 5 ปี</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										&nbsp;
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>   ก่อนรอบระยะบัญชีปีปัจจุบัน</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest23_year">
										<p class="CreateModel_interest23_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest23_pay">
										<p class="CreateModel_interest23_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest23_wht">
										<p class="CreateModel_interest23_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label> (2.4) กำไรที่รับรู้ทางบัญชีโดยวิธีส่วนได้เสีย (equity method)</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest24_year">
										<p class="CreateModel_interest24_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest24_pay">
										<p class="CreateModel_interest24_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest24_wht">
										<p class="CreateModel_interest24_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label> (2.5) อื่น ๆ (ระบุ)</label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest25_comment">
										<p class="CreateModel_interest25_comment require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest25_year">
										<p class="CreateModel_interest25_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest25_pay">
										<p class="CreateModel_interest25_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.interest25_wht">
										<p class="CreateModel_interest25_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label> 5. การจ่ายเงินได้ที่ต้องหักภาษี ณ ที่จ่าย ตามคำสั่งกรมสรรพากรที่ออกตาม</label> 
									</div> 
									<div class="col-lg-2 col-md-2 col-xs-2">
											&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
											&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
											&nbsp;
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>มาตรา 3 เตรส (ระบุ)</label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control">
									</div>
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.revenue_year">
										<p class="CreateModel_revenue_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.revenue_pay">
										<p class="CreateModel_revenue_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.revenue_wht">
										<p class="CreateModel_revenue_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"  ng-show="fullset">
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label>  (เช่น รางวัล ส่วนลดหรือประโยชน์ใด ๆ เนื่องจากการส่งเสริมการขาย<br/>รางวัล ในการประกวด การแข่งขัน การชิงโชค ค่าแสดงของนักแสดงสาธารณะ ค่าจ้าง <br/>ทำของ ค่าโฆษณา ค่าเช่า ค่าขนส่ง ค่าบริการ ค่าเบี้ยประกันวินาศภัย ฯลฯ)</label> 
									</div>											
									<div class="col-lg-2 col-md-2 col-xs-2">
											&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
											&nbsp;
									</div><div class="col-lg-2 col-md-2 col-xs-2">
											&nbsp;
									</div> 
								</div><div class="form-group col-lg-12"  >
									<div class="col-lg-2 col-md-2 col-xs-2">
										<label>6. อื่น ๆระบุ</label> 
									</div><div class="col-lg-4 col-md-4 col-xs-4">
										<input class="form-control" ng-model="CreateModel.other_comment">
										<p class="CreateModel_other_comment require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
									<div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.other_year">
										<p class="CreateModel_other_year require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.other_pay">
										<p class="CreateModel_other_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.other_wht">
										<p class="CreateModel_other_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12" style="padding-top: 70px;"> 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>ประกันสังคม</label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.social">
										<p class="CreateModel_social require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
									<div class="col-lg-4 col-md-4 col-xs-4 text-right" >
										<label>กองทุนสำรองเลี้ยงชีพ</label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.fund"  >
										<p class="CreateModel_fund require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12" > 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>&nbsp;</label> 
									</div>
									<div class="col-lg-4 col-md-4 col-xs-4 text-right" >
										<label>รวมเงินที่จ่ายและภาษีที่หักนำส่ง</label> 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.total_pay">
										<p class="CreateModel_total_pay require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.total_wht" ng-change="loadAlphabet()">
										<p class="CreateModel_total_wht require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12"> 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>รวมเงินภาษีที่หักนำส่ง (ตัวอักษร) </label> 
									</div> 
									<div class="col-lg-8 col-md-8 col-xs-8">
										<input class="form-control" ng-model="CreateModel.total_alphabet">
										<p class="CreateModel_alphabet require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div> 
								</div><div class="form-group col-lg-12" ng-show="fullset"> 
									<div class="col-lg-4 col-md-4 col-xs-4">
										<label>รวมเงินภาษีที่หักนำส่ง (ตัวอักษร) </label> 
									</div> 
									<div class="col-lg-8 col-md-8 col-xs-8">
										<label><input type="checkbox" ng-model="onetime_vat" >&nbsp; (1) ออกภาษีให้ครั้งเดียว</label>&nbsp; 
										<label><input type="checkbox" ng-model="forever_vat">&nbsp; (2) ออกภาษีให้ตลอดไป</label>&nbsp; 
										<label><input type="checkbox" ng-model="wht_vat" >&nbsp; (3) หักภาษี ณ ที่จ่าย </label>&nbsp; 
										<label><input type="checkbox" ng-model="other_vat">&nbsp; (4)  อื่น ๆ …………………</label>
									</div> 
								</div><div class="form-group col-lg-12" ng-show="fullset"> 
									<div class="col-lg-10 col-md-10 col-xs-10">
										&nbsp; 
									</div><div class="col-lg-2 col-md-2 col-xs-2">
										<input class="form-control" ng-model="CreateModel.other_vat_comment">
										<p class="CreateModel_other_vat_comment require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
								</div><div class="form-group col-lg-12"> 
									<div class="col-lg-6 col-md-6 col-xs-6">
										&nbsp;
									</div>											
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label> ลงชื่อผู้จ่ายเงิน </label> 
										<input class="form-control" ng-model="CreateModel.sign_name">
										<p class="CreateModel_sign_name require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
								</div><div class="form-group col-lg-12"> 
									<div class="col-lg-6 col-md-6 col-xs-6">
										&nbsp;
									</div>											
									<div class="col-lg-6 col-md-6 col-xs-6">
										<label> วัน   เดือน   ปี  ที่ออกหนังสือรับรอง </label> 
										<input class="form-control" ng-model="CreateModel.sign_date"  data-date-format="dd-MM-yyyy" bs-datepicker>
										<p class="CreateModel_sign_date require text-danger"><?php echo $this->lang->line('Require');?></p>
									</div>
								</div>
								<div class="form-group text-right">
									<br/><br/>
									<div class="col-lg-12 col-md-12 col-xs-12">
									<button class="btn btn-primary"  ng-click="onSaveTagClick()"><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
									<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
									</div>
								</div>
							</div>
						</div>  
					</div>
					<!-- /.row (nested) -->
				
				</div>
				
				 
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.create vat -->
		
		<!-- /List.row types-->
		<div class="row DisplayDevice" >
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<?php echo $this->lang->line('ListOfWHT');?>
					</div> 
					<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-xs-12">
						<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
						<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
					</div>
					<div class="col-lg-12 col-md-12 col-xs-12">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr> 
										<th><?php echo $this->lang->line('BookNo');?></th>
										<th><?php echo $this->lang->line('NumNo');?></th>
										<th><?php echo $this->lang->line('Project');?></th> 
										<th><?php echo $this->lang->line('CompanyNameEx2');?></th> 
										<th><?php echo $this->lang->line('PayDate');?></th>
										<th><?php echo $this->lang->line('PayPrices');?></th> 
										<th><?php echo $this->lang->line('withHoldingTax');?></th> 
										<th><?php echo $this->lang->line('Option');?></th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="item in modelDeviceList">
										<td ng-bind="item.book_no"></td> 
										<td ng-bind="item.num_no"></td>
										<td ng-bind="item.project_name"></td>
										<td ng-bind="item.com_name"></td>
										<td ng-bind="item.sign_date"></td>
										<td ng-bind="item.total_pay" class="text-right"></td>
										<td ng-bind="item.total_wht" class="text-right"></td>
										<td>
											<button ng-click="printPDF(item)" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-print"></i> <span class="hidden-xs"><?php echo $this->lang->line('Print');?></span></button>
											<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
											<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											 
										</td> 
									</tr>
								</tbody>
							</table>
						</div>
						<!-- /.table-responsive -->
					</div>
					
					  <!-- ทำหน้า -->
						<div class="row tblResult small"  >
							<div class="col-md-7 col-sm-7 col-xs-12 ">
								<label class="col-md-4 col-sm-4 col-xs-12">
									<?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
								</label>
								<label class="col-md-4 col-sm-4 col-xs-12">
									<?php echo $this->lang->line('ResultsPerPage');?>
								</label>
								<div class="col-md-4 col-sm-4 col-xs-12 ">
									<ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
										<ui-select-match>{{$select.selected.Value}}</ui-select-match>
										<ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
											<span ng-bind-html="pSize.Text | highlight: $select.search"></span>
										</ui-select-choices>
									</ui-select>
								</div>
							</div>
							<div class="col-md-5 col-sm-5 col-xs-12  ">
								<label class="col-md-4 col-sm-4 col-xs-12">
									<span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
								</label>
								<div class="col-md-3 col-sm-3 col-xs-12">
									<ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
										<ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
										<ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
											<span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
										</ui-select-choices>
									</ui-select>
								</div>
								<label class="col-md-4 col-sm-4 col-xs-12">
									/ {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
								</label>
							</div>
						</div>
						<!-- ทำหน้า -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /List.row types-->
			
		 
		 
</div>
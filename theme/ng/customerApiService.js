'use strict';
myApp.factory('customerApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Customer/");
    return {
          
        //Customer 
	    listCustomer: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCustomerModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getCustomer: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCustomerModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveCustomer: function (model, onComplete) {
            baseService.postObject(servicebase + 'addCustomer', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteCustomer: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteCustomer', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCustomerComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
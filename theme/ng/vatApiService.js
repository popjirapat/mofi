'use strict';
myApp.factory('vatApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("vat/");
    return {
          
        //vat 
	    listvat: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVatModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getvat: function (model, onComplete) {
            baseService.postObject(servicebase + 'getVatModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savevat: function (model, onComplete) {
            baseService.postObject(servicebase + 'addVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletevat: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteVat', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getVatComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
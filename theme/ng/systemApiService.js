'use strict';
myApp.factory('systemApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("SystemConfig/");
    return {
          
        //Company 
	    listCompany: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCompanyModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getCompany: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCompanyModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveCompany: function (model, onComplete) {
            baseService.postObject(servicebase + 'addCompany', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteCompany: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteCompanyname', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
        
    };
}]);
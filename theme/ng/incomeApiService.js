'use strict';
myApp.factory('incomeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("income/");
    return {
          
        //income 
	    listincome: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getIncomeModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getincome: function (model, onComplete) {
            baseService.postObject(servicebase + 'getIncomeModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveincome: function (model, onComplete) {
            baseService.postObject(servicebase + 'addIncome', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteincome: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteIncome', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getIncomeComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
	    listincomeDetail: function (model, onComplete) {
            baseService.postObject(servicebase + 'getIncomeDetailList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);
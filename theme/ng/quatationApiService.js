'use strict';
myApp.factory('quatationApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("quatation/");
    return {
          
        //quatation 
	    listquatation: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getQuatationModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getquatation: function (model, onComplete) {
            baseService.postObject(servicebase + 'getQuatationModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savequatation: function (model, onComplete) {
            baseService.postObject(servicebase + 'addQuatation', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletequatation: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteQuatation', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		updatestatusquatation: function (model, onComplete) {
            baseService.postObject(servicebase + 'UpdateStatusQuatation', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        }, 
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getQuatationComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
	    listquatationDetail: function (model, onComplete) {
            baseService.postObject(servicebase + 'getQuatationDetailList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);